package pl.projects.ppetka.mapexample.network;

import io.reactivex.Single;
import pl.projects.ppetka.mapexample.network.interfaces.Repository;
import pl.projects.ppetka.mapexample.network.interfaces.RetrofitInterface;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.Autocomplete;

/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public class NetworkRepository implements Repository {
    private static RetrofitInterface api;
    private static NetworkRepository networkRepository;

    public static NetworkRepository getRepository() {
        if (networkRepository == null) {
            networkRepository = new NetworkRepository();
            networkRepository.setApi(RetrofitProvider.getRetrofitInstance().create(RetrofitInterface.class));
        }
        return networkRepository;
    }

    private void setApi(RetrofitInterface api) {
        NetworkRepository.api = api;
    }


    @Override
    public Single<Boolean> getNearbyPlaces() {
        return api.getNearbyPlaces();
    }

    @Override
    public Single<Autocomplete> getAutocompletePredictions(String input, Geolocation geolocation, Integer radius, String googleApiKey, String language) {
        return api.getAutocompletePredictions(input, geolocation, radius, googleApiKey, language);
    }
}