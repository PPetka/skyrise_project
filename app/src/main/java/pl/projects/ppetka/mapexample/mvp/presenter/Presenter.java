package pl.projects.ppetka.mapexample.mvp.presenter;

import pl.projects.ppetka.mapexample.mvp.view.View;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public interface Presenter<T extends View> {
    void attachView(T view);
    void onStop();
}
