package pl.projects.ppetka.mapexample.network.interfaces;

import io.reactivex.Single;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.Autocomplete;


/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public interface Repository {

    Single<Boolean> getNearbyPlaces();

    Single<Autocomplete> getAutocompletePredictions(String keyword, Geolocation location, Integer radius, String googleApiKey, String language);
}