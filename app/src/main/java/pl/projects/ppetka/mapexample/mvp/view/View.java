package pl.projects.ppetka.mapexample.mvp.view;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public interface View {
    void showError(String message);
}
