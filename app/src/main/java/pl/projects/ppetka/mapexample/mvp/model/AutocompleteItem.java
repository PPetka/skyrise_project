package pl.projects.ppetka.mapexample.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public class AutocompleteItem implements Parcelable {
    private String place_id;
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public static Creator<AutocompleteItem> getCREATOR() {
        return CREATOR;
    }


    protected AutocompleteItem(Parcel in) {
        description = in.readString();

        place_id = in.readString();
    }

    public static final Creator<AutocompleteItem> CREATOR = new Creator<AutocompleteItem>() {
        @Override
        public AutocompleteItem createFromParcel(Parcel in) {
            return new AutocompleteItem(in);
        }

        @Override
        public AutocompleteItem[] newArray(int size) {
            return new AutocompleteItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(description);
        dest.writeString(place_id);
    }
}
