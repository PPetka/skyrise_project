package pl.projects.ppetka.mapexample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import pl.projects.ppetka.mapexample.R;
import pl.projects.ppetka.mapexample.mvp.model.AutocompleteItem;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public class AutocompleteDropDownAdapter extends RecyclerView.Adapter<AutocompleteDropDownAdapter.ViewHolder> {
    private List<AutocompleteItem> autocompleteList;
    private DropdownItemClickListener dropdownItemClickListener;

    public interface DropdownItemClickListener {
        void onDropdownItemClicked(AutocompleteItem itemText);
    }

    public AutocompleteDropDownAdapter(List<AutocompleteItem> autocompleteList) {
        this.autocompleteList = autocompleteList;
    }

    public void updateAndNotify(List<AutocompleteItem> autocopleteList) {
        this.autocompleteList.clear();
        this.autocompleteList.addAll(autocopleteList);
        notifyDataSetChanged();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.signle_drop_down_item, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final AutocompleteItem autocompleteItem = autocompleteList.get(position);
        holder.mTextView.setText(autocompleteItem.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dropdownItemClickListener != null) {
                    dropdownItemClickListener.onDropdownItemClicked(autocompleteItem);
                    autocompleteList.clear();
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return autocompleteList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.item_text);
        }
    }

    public void setDropdownItemClickListener(DropdownItemClickListener dropdownItemClickListener) {
        this.dropdownItemClickListener = dropdownItemClickListener;
    }
}
