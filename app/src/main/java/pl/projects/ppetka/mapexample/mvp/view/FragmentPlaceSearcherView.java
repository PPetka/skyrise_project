package pl.projects.ppetka.mapexample.mvp.view;

import java.util.List;

import pl.projects.ppetka.mapexample.mvp.model.AutocompleteItem;

/**
 * Created by Przemysław Petka on 11/25/2017.
 */

public interface FragmentPlaceSearcherView extends View {
    void onAutocompleteKeysFound(List<AutocompleteItem> predecations);
}
