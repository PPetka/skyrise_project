package pl.projects.ppetka.mapexample.network.interfaces;

import io.reactivex.Single;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.Autocomplete;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public interface RetrofitInterface {

    @GET("/maps/api/place/nearbysearch/json?location=54.372158,18.638306&radius=10000&&&keyword=c&key=AIzaSyDrr177ZcD7mMIZMExn7186XXM4kY2MItw")
    Single<Boolean> getNearbyPlaces();

    //    @GET("/maps/api/place/autocomplete/json?location=54.372158,18.638306&radius=10000&language=pl&key=AIzaSyDrr177ZcD7mMIZMExn7186XXM4kY2MItw")
    @GET("/maps/api/place/autocomplete/json?&strictbounds")
    Single<Autocomplete> getAutocompletePredictions(@Query("input") String input, @Query("location") Geolocation location, @Query("radius") Integer radius, @Query("key") String googleApiKey, @Query("language") String language);

}
