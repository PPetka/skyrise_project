package pl.projects.ppetka.mapexample.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.projects.ppetka.mapexample.R;
import pl.projects.ppetka.mapexample.fragment.SearchPlacesFragment;
import pl.projects.ppetka.mapexample.fragment.SearchPlacesFragment_;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.PlaceModel;


@EActivity(R.layout.activity_main)
public class MainActivity extends LocationAwareActivity implements OnMapReadyCallback {
    private static final String TAG = "MainActivity";

    private SearchPlacesFragment searchPlacesFragment;
    private GoogleMap googleMap;

    @FragmentById(R.id.map)
    protected SupportMapFragment mapFragment;

    @ViewById(R.id.dummy_focus_catch_view)
    protected EditText dummyEditText;

    @InstanceState
    protected PlaceModel currentSearchResultPlace;

    @InstanceState
    protected Geolocation myLastLocation;

    private Marker myCurrentLocationMarker;
    private MarkerOptions myCurrentLocationMarkerOptions;

    @AfterViews
    public void initGui() {
        mapFragment.getMapAsync(this);

        searchPlacesFragment = new SearchPlacesFragment_();

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fragment_search_container, searchPlacesFragment).commit();

        searchPlacesFragment.setSearchPlacesFragmentListener(new SearchPlacesFragment.SearchPlacesFragmentListener() {
            @Override
            public void onPlaceFound(final Place foundPlace) {
                currentSearchResultPlace = new PlaceModel(foundPlace);
                addMarkersToMapAndCenterCamera(Collections.singletonList(foundPlace));
            }

            @Override
            public void onAutocompletePlacesFound(List<Place> places) {
                addMarkersToMapAndCenterCamera(places);
            }

            @Override
            public void onSearchBoxClicked() {
                if (googleMap != null) {
                    clearOldMarkers();
                }
            }
        });

        dummyEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftKeyboard(view);
                }
            }
        });
        hideSoftKeyboard(dummyEditText);
    }

    private void addMarkersToMapAndCenterCamera(List<Place> places) {
        if (googleMap != null) {
            clearOldMarkers();

            List<MarkerOptions> markerOptionsList = new ArrayList<>();
            for (Place place : places) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(place.getLatLng())
                        .title(place.getName().toString());
                markerOptionsList.add(markerOptions);
                googleMap.addMarker(markerOptions);
            }

            LatLngBounds latLngBounds = calculateMultipleMarkersLatLngBounds(markerOptionsList);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds, 200);

            googleMap.animateCamera(cameraUpdate);
        }
    }

    private LatLngBounds calculateMultipleMarkersLatLngBounds(List<MarkerOptions> markerOptions) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (MarkerOptions marker : markerOptions) {
            builder.include(marker.getPosition());
        }
        return builder.build();
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (myCurrentLocationMarkerOptions == null) {
            double lat = Double.parseDouble(getResources().getString(R.string.default_lat));
            double lng = Double.parseDouble(getResources().getString(R.string.default_lng));
            myCurrentLocationMarkerOptions = new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .icon(bitmapDescriptorFromVectorDrawable(this, R.drawable.ic_place));
        }
        //add my location marker
        if (currentSearchResultPlace != null) {
            List<Place> placeList = new ArrayList<>();
            placeList.add(currentSearchResultPlace);
            addMarkersToMapAndCenterCamera(placeList);
            searchPlacesFragment.setPrimaryEditText(currentSearchResultPlace.getName().toString());
        }
    }

    @Override
    protected void onLocationChangedListener(Geolocation latLng) {
        Log.e(TAG, "lat: " + latLng.latitude + " lng: " + latLng.longitude);
        myLastLocation = latLng;
        //initialize my location marker for the first time
        myCurrentLocationMarker = googleMap.addMarker(myCurrentLocationMarkerOptions);
        if (myLastLocation != null) {
            myCurrentLocationMarker.setPosition(new LatLng(myLastLocation.latitude, myLastLocation.longitude));
        }

        if (searchPlacesFragment != null) {
            searchPlacesFragment.updateGeolocation(latLng);
        }
    }

    private void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void clearOldMarkers() {
        googleMap.clear();
        myCurrentLocationMarker = googleMap.addMarker(myCurrentLocationMarkerOptions);
        if (myLastLocation != null) {
            myCurrentLocationMarker.setPosition(new LatLng(myLastLocation.latitude, myLastLocation.longitude));
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVectorDrawable(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
}
