package pl.projects.ppetka.mapexample.network.usecase;

import io.reactivex.Single;
import pl.projects.ppetka.mapexample.network.interfaces.Repository;
import pl.projects.ppetka.mapexample.network.interfaces.Usecase;

/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public class NearbyPlacesUsecase implements Usecase<Boolean> {
    private Repository repository;

    public NearbyPlacesUsecase(Repository repository) {
        this.repository = repository;
    }

    @Override
    public Single<Boolean> execute() {
        return repository.getNearbyPlaces();
    }
}
