package pl.projects.ppetka.mapexample.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public class Geolocation implements Parcelable {
    public final double latitude;
    public final double longitude;

    public Geolocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    protected Geolocation(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<Geolocation> CREATOR = new Creator<Geolocation>() {
        @Override
        public Geolocation createFromParcel(Parcel in) {
            return new Geolocation(in);
        }

        @Override
        public Geolocation[] newArray(int size) {
            return new Geolocation[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }

    @Override
    public String toString(){
        return String.format("%.6f,%.6f", latitude, longitude);
    }
}
