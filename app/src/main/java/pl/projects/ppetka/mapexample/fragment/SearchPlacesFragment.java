package pl.projects.ppetka.mapexample.fragment;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import pl.projects.ppetka.mapexample.R;
import pl.projects.ppetka.mapexample.SimpleTextWatcher;
import pl.projects.ppetka.mapexample.adapter.AutocompleteDropDownAdapter;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.PlaceModel;
import pl.projects.ppetka.mapexample.mvp.model.AutocompleteItem;
import pl.projects.ppetka.mapexample.mvp.presenter.SearchPlacesPresenter;
import pl.projects.ppetka.mapexample.mvp.view.FragmentPlaceSearcherView;
import pl.projects.ppetka.mapexample.network.NetworkRepository;

/**
 * Created by Przemysław Petka on 11/25/2017.
 */

@EFragment(R.layout.fragment_place_searcher)
public class SearchPlacesFragment extends Fragment implements FragmentPlaceSearcherView {
    private static final String TAG = "SearchPlacesFragment";
    private static Integer MAX_RADIUS = 10000;
    private static final String LANGUAGE = "pl";

    private Geolocation location;
    private SearchPlacesPresenter presenter;
    private AutocompleteDropDownAdapter adapter;
    private GeoDataClient geoDataClient;
    private final Handler handler = new Handler();
    private Runnable pendingTask;

    //views
    @ViewById(R.id.primary_edit_text)
    protected EditText primaryEditText;
    @ViewById(R.id.root_card_view)
    protected CardView searchView;
    @ViewById(R.id.card_view_edit_text)
    protected EditText cardViewEditText;
    @ViewById(R.id.close_button)
    protected ImageView closeButton;
    @ViewById(R.id.primary_search_view)
    protected LinearLayout primarySearchView;
    @ViewById(R.id.drop_down_recycler_view)
    protected RecyclerView dropDownRv;

    private SearchPlacesFragmentListener searchPlacesFragmentListener;

    public interface SearchPlacesFragmentListener {
        void onPlaceFound(Place foundPlace);

        void onAutocompletePlacesFound(List<Place> places);

        void onSearchBoxClicked();
    }

    @AfterViews
    public void afterViews() {
        searchView.getBackground().setAlpha(getActivity().getResources().getInteger(R.integer.search_card_view_alpha));
        searchView.setVisibility(View.GONE);

        double lat = Double.parseDouble(getResources().getString(R.string.default_lat));
        double lng = Double.parseDouble(getResources().getString(R.string.default_lng));
        location = new Geolocation(lat, lng);

        //attach presenter
        presenter = new SearchPlacesPresenter(NetworkRepository.getRepository());
        presenter.attachView(this);

        geoDataClient = Places.getGeoDataClient(getActivity(), null);

        //initialize dropdown
        adapter = new AutocompleteDropDownAdapter(new ArrayList<AutocompleteItem>());
        adapter.setDropdownItemClickListener(new AutocompleteDropDownAdapter.DropdownItemClickListener() {
            @Override
            public void onDropdownItemClicked(AutocompleteItem itemText) {
                geoDataClient.getPlaceById(itemText.getPlace_id()).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                    @Override
                    public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                        if (task.isSuccessful()) {
                            if (searchPlacesFragmentListener != null) {
                                List<Place> places = getPlacesFromBufferResponse(task.getResult());
                                primaryEditText.setText(places.get(0).getName().toString());
                                searchPlacesFragmentListener.onPlaceFound(places.get(0));
                            }
                        }
                    }
                });
                primaryEditText.setText(itemText.getDescription());
                clearAndHideFloatingSearcher();
            }
        });

        dropDownRv.setAdapter(adapter);
        dropDownRv.setLayoutManager(new LinearLayoutManager(getActivity()));

        primaryEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showCardView();
                    if (searchPlacesFragmentListener != null) {
                        searchPlacesFragmentListener.onSearchBoxClicked();
                    }
                }
            }
        });

        cardViewEditText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(final Editable s) {
                handler.removeCallbacks(pendingTask);
                pendingTask = new Runnable() {
                    @Override
                    public void run() {
                        if (getActivity() != null) {
                            presenter.getAutocompleteKeywords(getActivity(), s.toString(), location, MAX_RADIUS, LANGUAGE);
                        }
                    }
                };
                handler.postDelayed(pendingTask, getActivity().getResources().getInteger(R.integer.search_hold_time));
            }
        });

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearAndHideFloatingSearcher();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    public void updateGeolocation(Geolocation geolocation) {
        this.location = geolocation;
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getActivity(), "Ups.. Something went wrong", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAutocompleteKeysFound(List<AutocompleteItem> predecations) {
        adapter.updateAndNotify(predecations);

        String[] placesIds = extractIdFromPredictions(predecations);
        if (placesIds.length > 0) {
            geoDataClient.getPlaceById(placesIds).addOnCompleteListener(new OnCompleteListener<PlaceBufferResponse>() {
                @Override
                public void onComplete(@NonNull Task<PlaceBufferResponse> task) {
                    if (task.isSuccessful()) {
                        if (searchPlacesFragmentListener != null) {
                            List<Place> places = getPlacesFromBufferResponse(task.getResult());
                            searchPlacesFragmentListener.onAutocompletePlacesFound(places);
                        }
                    }
                }
            });
        }
        primaryEditText.setText("");
    }

    public void showCardView() {
        searchView.setVisibility(View.VISIBLE);
        primarySearchView.setVisibility(View.GONE);

        cardViewEditText.requestFocus();
        //force keyboard show
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(cardViewEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    public void clearAndHideFloatingSearcher() {
        primarySearchView.setVisibility(View.VISIBLE);
        clearFloatingSearcher();
        searchView.setVisibility(View.GONE);
    }


    private void clearFloatingSearcher() {
        cardViewEditText.setText("");
        adapter.updateAndNotify(new ArrayList<AutocompleteItem>() {
        });
    }

    public void setSearchPlacesFragmentListener(SearchPlacesFragmentListener searchPlacesFragmentListener) {
        this.searchPlacesFragmentListener = searchPlacesFragmentListener;
    }

    private List<Place> getPlacesFromBufferResponse(PlaceBufferResponse placesBuffer) {
        List<Place> places = new ArrayList<>();
        if (placesBuffer != null) {
            for (int i = 0; i < placesBuffer.getCount(); i++) {
                places.add(new PlaceModel(placesBuffer.get(i)));
            }
            placesBuffer.release();
        }
        return places;
    }

    private String[] extractIdFromPredictions(List<AutocompleteItem> autocompleteItems) {
        String[] idArray = new String[autocompleteItems.size()];
        for (int i = 0; i < autocompleteItems.size(); i++) {
            idArray[i] = autocompleteItems.get(i).getPlace_id();
        }
        return idArray;
    }

    public void setPrimaryEditText(String text) {
        if (primaryEditText != null) {
            primaryEditText.setText(text);
        }
    }
}
