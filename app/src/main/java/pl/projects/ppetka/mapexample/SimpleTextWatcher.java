package pl.projects.ppetka.mapexample;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by Przemysław Petka on 11/25/2017.
 */

public class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
