package pl.projects.ppetka.mapexample.mvp.presenter;

import android.content.Context;
import android.widget.Toast;


import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.AutocompleteItem;
import pl.projects.ppetka.mapexample.mvp.model.Autocomplete;
import pl.projects.ppetka.mapexample.mvp.view.FragmentPlaceSearcherView;
import pl.projects.ppetka.mapexample.network.interfaces.Repository;
import pl.projects.ppetka.mapexample.network.usecase.AutocompletePlacesNameUsecase;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public class SearchPlacesPresenter implements Presenter<FragmentPlaceSearcherView> {
    private FragmentPlaceSearcherView view;
    private Repository repository;
    private Disposable disposable;

    public SearchPlacesPresenter(Repository repository) {
        this.repository = repository;
    }

    public void getAutocompleteKeywords(final Context ctx, String keyword, Geolocation geolocation, Integer maxRadius, String language) {
        if (!keyword.isEmpty()) {
            AutocompletePlacesNameUsecase usecase = new AutocompletePlacesNameUsecase(ctx, repository, keyword, geolocation, maxRadius, language);
            usecase.execute()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(new Function<Autocomplete, List<AutocompleteItem>>() {
                        @Override
                        public List<AutocompleteItem> apply(Autocomplete autocomplete) throws Exception {
                            return autocomplete.getPredictions();
                        }
                    })
                    .toObservable()
                    .flatMapIterable(new Function<List<AutocompleteItem>, Iterable<AutocompleteItem>>() {
                        @Override
                        public Iterable<AutocompleteItem> apply(List<AutocompleteItem> autocompleteItems) throws Exception {
                            return autocompleteItems;
                        }
                    })
                    .take(3)
                    .toList()
                    .subscribe(new SingleObserver<List<AutocompleteItem>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable = d;
                        }

                        @Override
                        public void onSuccess(List<AutocompleteItem> autocompleteItems) {
                            if (autocompleteItems != null) {
                                view.onAutocompleteKeysFound(autocompleteItems);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.showError(e.toString());
                        }
                    });

        }
    }

    @Override
    public void attachView(FragmentPlaceSearcherView view) {
        this.view = view;
    }

    @Override
    public void onStop() {
        if (disposable != null) {
            disposable.dispose();
        }
    }
}

