package pl.projects.ppetka.mapexample.network.usecase;

import android.content.Context;

import io.reactivex.Single;
import pl.projects.ppetka.mapexample.R;
import pl.projects.ppetka.mapexample.network.interfaces.Repository;
import pl.projects.ppetka.mapexample.network.interfaces.Usecase;
import pl.projects.ppetka.mapexample.mvp.model.Geolocation;
import pl.projects.ppetka.mapexample.mvp.model.Autocomplete;

/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public class AutocompletePlacesNameUsecase implements Usecase<Autocomplete> {
    private Repository repository;
    private String input;
    private Geolocation location;
    private Integer radius;
    private Context ctx;

    public AutocompletePlacesNameUsecase(Context ctx, Repository repository, String input, Geolocation location, Integer radius, String language) {
        this.ctx = ctx;
        this.repository = repository;
        this.input = input;
        this.location = location;
        this.radius = radius;
        this.language = language;
    }

    private String language;

    @Override
    public Single<Autocomplete> execute() {
        return repository.getAutocompletePredictions(input, location, radius, ctx.getResources().getString(R.string.google_maps_api_key), language);
    }
}
