package pl.projects.ppetka.mapexample.network.interfaces;

import io.reactivex.Single;

/**
 * Created by Przemysław Petka on 11/22/2017.
 */

public interface Usecase<T> {
    Single<T> execute();
}
