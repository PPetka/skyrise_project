package pl.projects.ppetka.mapexample.mvp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Przemysław Petka on 11/23/2017.
 */

public class Autocomplete implements Parcelable {
    private List<AutocompleteItem> predictions;

    private Autocomplete(Parcel in) {
        predictions = in.createTypedArrayList(AutocompleteItem.CREATOR);
    }

    public static final Creator<Autocomplete> CREATOR = new Creator<Autocomplete>() {
        @Override
        public Autocomplete createFromParcel(Parcel in) {
            return new Autocomplete(in);
        }

        @Override
        public Autocomplete[] newArray(int size) {
            return new Autocomplete[size];
        }
    };

    public List<AutocompleteItem> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<AutocompleteItem> predictions) {
        this.predictions = predictions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(predictions);
    }
}
